import ru.fbtw.utils.ArrayUtils;
import ru.fbtw.utils.Pair;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class Main {
	private static int testId = 0;

	public static void main(String[] args) {
		System.out.println("Результаты претестов: \n");

		testSolution(1, 2, 3, 4, 5, 6, 78, 9); // тест 0: все числа различнык
		testSolution(4, 3, 2, 1, 3, 5, 6); // тест 1: длинный конец
		testSolution(4, 3, 2, 1, 3); // тест 2: длинное начало
		testSolution(3, 2, 1, 3); // тест 3: равная длина
		testSolution(1); // тест 4 : один элемент
		testSolution(); // тест 5: нет элементов
		testSolution(1,1,1); // тест 6: все одинаковые
		testSolution(3,4,3,4,3,4);// тест 7: повторяющиеся одинаковые последовательности

		System.out.println("Результаты случайных тестов: \n");
		testSolution(ArrayUtils.randomArray(10));
		testSolution(ArrayUtils.randomArray(20));
		testSolution(ArrayUtils.randomArray(15));

		testSolution(ArrayUtils.readIntArray("для теста"));


	}


	private static Pair<Integer, Integer> solution(int[] arr) {
		int startIndex = 0, length = 0, maxLength = 0, maxIndex = 0;
		boolean flag = true;

		for (int i = 0; i < arr.length; i++) {
			for (int j = startIndex; j < i; j++) {
				if (arr[j] == arr[i]) {
					if (length >= maxLength) {
						maxLength = length;
						maxIndex = startIndex;
					}

					startIndex = j + 1;
					length = i - j;
					flag = false;

					break;
				}
			}

			if (flag) {
				length++;
			} else {
				flag = true;
			}
		}

		if (length >= maxLength) {
			maxLength = length;
			maxIndex = startIndex;
		}

		if(maxLength == 0){
			maxIndex = -1;
		}

		return new Pair<>(maxIndex, maxLength);
	}

	/*private static void hashSolution(int[] arr){
		LinkedHashSet<Integer> uniqueNums = new LinkedHashSet<>();
		int startIndex = 0, length = 0, maxLength = 0, maxIndex = 0;


	}*/

	private static void printSolution(Pair<Integer, Integer> result) {
		System.out.printf("Индекс начала: %d Длинна: %d\n\n", result.getFirst(), result.getSecond());
	}

	private static void testSolution(int... values) {
		System.out.printf("Тест №%d Массив: %s\n", testId, Arrays.toString(values));

		Pair<Integer, Integer> testResult = solution(values);
		printSolution(testResult);

		testId++;
	}


}
